import Vue from "vue";
import { router } from "@/router/index.js";
import { userLogout  } from "@/services/userService.js";

import axios from "axios";
const instance = axios.create();

instance.interceptors.response.use(
  function(response) {
    Promise.resolve(response);
  },
  function(err) {
    //Not getting status in error
      if(err.response.status === 401){
        userLogout();
          // router.push('/login')
      }
    return Promise.reject(err);
  }
);

Vue.use(instance);

export default axios;
